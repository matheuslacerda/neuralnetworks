from flask import Flask, request
import sys
import numpy as np
import io
import base64
from PIL import Image
from keras.preprocessing.image import ImageDataGenerator
from keras.models import load_model

app = Flask(__name__)

wsgi_app = app.wsgi_app

label_list = ['1', '2', '3', '4', '5', '6', '7', '8', '9',
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
			'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
best_model = load_model('model_captcha.hdf5')
test_datagen = ImageDataGenerator(
	samplewise_center=True,
	samplewise_std_normalization=True,
	rescale=1. / 255)

    
def process_captcha(image):
    img_w, img_h = image.size
    background = Image.new('RGBA', (50, 50), (255, 255, 255, 255))
    bg_w, bg_h = background.size
    offset = ((bg_w - img_w) // 2, (bg_h - img_h) // 2)
    background.paste(image, offset)

    return np.asarray(background)


@app.route('/', methods = ['POST'])
def decode():
    img = Image.open(io.BytesIO(base64.b64decode(request.form['imagem'])))

    imgs = [process_captcha(img.crop((0, 0, 36 + 5, 50))),
		    process_captcha(img.crop((36 - 5, 0, 65 + 5, 50))),
		    process_captcha(img.crop((65 - 5, 0, 97 + 5, 50))),
		    process_captcha(img.crop((97 - 5, 0, 125 + 5, 50))),
		    process_captcha(img.crop((125 - 5, 0, 155 + 5, 50))),
		    process_captcha(img.crop((155 - 5, 0, 180, 50)))]
    imgs = np.array(imgs)

    global test_datagen
    test_generator = test_datagen.flow(imgs, np.zeros(6), batch_size=6, shuffle=False, seed=42)

    global best_model
    y_preds = best_model.predict_generator(test_generator, steps=6)

    captcha_break = ''
    global label_list
    for i in range(6):
        captcha_break += label_list[np.argmax(y_preds[i])]

    return captcha_break


if __name__ == '__main__':
    import os
    HOST = os.environ.get('SERVER_HOST', 'localhost')
    try:
        PORT = int(os.environ.get('SERVER_PORT', '5555'))
    except ValueError:
        PORT = 5555
    app.run(HOST, PORT)
