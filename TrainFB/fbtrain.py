import os
import numpy as np
from PIL import Image

import keras
from keras.models import Model
from keras.layers import Input, Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ModelCheckpoint


files = np.array(os.listdir('facebook3'))
files_test = np.array(os.listdir('facebook4'))
label_list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
              'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
              'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '_']

X_train = np.zeros((len(files), 71, 280, 3))
y_train = np.zeros((7, len(files), 37))
for i, file in enumerate(files):
    X_train[i] = Image.open('facebook3/' + file)
    for j in range(0, 6):
        y_train[j][i][label_list.index(file.lower()[j])] = 1

X_test = np.zeros((len(files_test), 71, 280, 3))
y_test = np.zeros((7, len(files_test), 37))
for i, file in enumerate(files_test):
    X_test[i] = Image.open('facebook4/' + file)
    for j in range(0, 6):
        y_test[j][i][label_list.index(file.lower()[j])] = 1

batch_size = 16

train_datagen = ImageDataGenerator(
        samplewise_center=True,
        samplewise_std_normalization=True,
        shear_range=0.2,
        zoom_range=0.2,
        rescale=1./255)

test_datagen = ImageDataGenerator(
        samplewise_center=True,
        samplewise_std_normalization=True,
        rescale=1./255)

input_shape = (71, 280, 3)
inputs = Input(shape=input_shape)
y = Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=input_shape)(inputs)
y = Conv2D(32, (3, 3), activation='relu')(y)
y = MaxPooling2D(pool_size=(2, 2), data_format="channels_first")(y)
y = Conv2D(64, (3, 3), activation='relu')(y)
y = Conv2D(64, (3, 3), activation='relu')(y)
y = MaxPooling2D(pool_size=(2, 2), data_format="channels_first")(y)
y = Conv2D(128, (3, 3), activation='relu')(y)
y = Conv2D(128, (3, 3), activation='relu')(y)
y = MaxPooling2D(pool_size=(2, 2), data_format="channels_first")(y)
y = Conv2D(256, (3, 3), activation='relu')(y)
y = Conv2D(256, (3, 3), activation='relu')(y)
y = MaxPooling2D(pool_size=(2, 2), data_format="channels_first")(y)
y = Flatten()(y)
y = Dropout(0.2)(y)
output1 = Dense(37, activation="softmax", name="output1")(y)
output2 = Dense(37, activation="softmax", name="output2")(y)
output3 = Dense(37, activation="softmax", name="output3")(y)
output4 = Dense(37, activation="softmax", name="output4")(y)
output5 = Dense(37, activation="softmax", name="output5")(y)
output6 = Dense(37, activation="softmax", name="output6")(y)
output7 = Dense(37, activation="softmax", name="output7")(y)

model = Model(input=inputs, output=[output1, output2, output3, output4, output5, output6, output7])

model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adadelta(),
              metrics=['accuracy'])

checkpointer = ModelCheckpoint('model_facebook.hdf5', verbose=1, save_best_only=True)

model.fit(
    X_train,
    [y_train[0], y_train[1], y_train[2], y_train[3], y_train[4], y_train[5], y_train[6]],
    validation_data=(X_test, [y_test[0], y_test[1], y_test[2], y_test[3], y_test[4], y_test[5], y_test[6]]),
    epochs=200,
    batch_size=32,
    callbacks=[checkpointer],
)
